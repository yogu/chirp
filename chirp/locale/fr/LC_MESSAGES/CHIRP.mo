��    '     T	  �  �      �  
  �  -   �  +   �     &     -  ^   <     �     �     �  	   �     �     �     �     �                    #     )     -     5     L  ]   S  J   �     �               2     A     S  (   a     �    �  ,   �     �     �     �       
                   7  k   @     �     �     �  
   �     �  	   �     �     �     �  	   �     	               !  A   0     r     �     �     �     �     �     �      �     �                 3   C   I      �      �      �      �      �      �      �      !     '!     E!     X!     i!     �!     �!     �!     �!     �!     �!  u   �!     o"     �"     �"     �"     �"     �"  1   �"     #  	   #     #     #  .   0#  	   _#     i#     n#     #     �#     �#     �#  
   �#     �#     �#  7   �#      $     $     $     2$     8$     =$     I$     Z$     p$  '   �$     �$     �$     �$  "   �$      %     %      %     %%     .%     @%     L%     X%  4   e%     �%     �%     �%  y  �%     T'  	   e'     o'     x'  %   �'     �'     �'     �'     �'     �'     �'     �'     (  	   7(     A(  
   I(     T(     j(     (     �(  
   �(     �(     �(     �(     �(     �(      �(     )     0)     5)     A)     S)     _)     m)     |)     �)     �)     �)     �)      �)      *  �   *     �*     �*  3   �*  *   �*  (   +  &   1+  ?   X+  �  �+  (  ]-    �.     �/  $   �/     �/     �/  !   �/     �/     0  
   0     0     #0     00     80     >0     Y0  �   k0  ;   1     P1     a1     u1     �1     �1     �1  �   �1     q2     �2     �2     �2  	   �2     �2     �2     �2     �2     �2     3     3     3     &3     63     N3  .   `3     �3     �3     �3     �3     �3     �3     �3  �  �3  �   �6  �   �7  ^  <8     �9  �   �9  G   b:  #   �:  =   �:  �   ;     �;     <  &  -<  �   T=     C>  	   H>     R>     _>     k>  L   {>  ,   �>     �>     ?     1?  �   A?  "   �?     @     .@  /   <@     l@     �@     �@     �@     �@     �@     �@     �@     A  '   0A     XA     vA     }A     �A     �A     �A     �A     �A  7   �A  !   �A     B     B  
   B     B     'B     /B  T  >B  ;  �C  ,   �D  7   �D     4E     ;E  l   IE     �E     �E     �E     �E     �E     �E     F     F     ;F     OF     bF     iF  	   qF  
   {F     �F     �F  q   �F  X   G     wG     �G     �G     �G     �G     �G  2   �G     H  X  H  2   vI     �I  %   �I  %   �I     �I     J     J     J     8J  x   @J     �J     �J     �J  
   �J     �J     �J     K     K     K  
   'K     2K     @K  	   DK     NK  ]   _K     �K     �K     �K  #   �K  	   L     %L     .L  0   JL     {L  "   �L  %   �L     �L  R   �L     BM  #   IM  %   mM     �M     �M     �M     �M  $   �M  (   	N     2N     ON  ,   eN  %   �N     �N  1   �N     �N     O     ;O  �   AO  -   �O     �O     P     +P     BP     KP  Q   SP  
   �P     �P     �P     �P  6   �P  	   #Q     -Q     2Q     NQ     aQ     uQ     ~Q     �Q     �Q     �Q  L   �Q     �Q     R     "R     8R     >R     CR     OR  "   gR     �R  -   �R     �R     �R     �R  '   S     5S     IS     ZS     _S     hS     �S     �S     �S  A   �S     �S  $   T  '   2T  �  ZT     V  	   *V     4V  #   =V  6   aV  '   �V     �V     �V     �V     �V     �V     �V     W  	   W     %W     ,W     =W     YW     tW  '   �W     �W     �W     �W     �W     �W     �W  4   X  *   HX     sX     zX     �X     �X     �X     �X      �X  '   Y     )Y     >Y     MY  !   `Y     �Y  �   �Y     =Z     EZ  8   LZ  1   �Z  /   �Z  *   �Z  E   [  @  X[  R  �]  ^  �^     K`  &   e`     �`  	   �`  0   �`     �`  
   �`  
   �`     �`     a     a     "a  ,   (a     Ua  �   na  ?   'b     gb     �b     �b     �b     �b     �b    �b     d     d     6d     Bd     _d     sd     �d      �d     �d     �d     �d     �d     e  '   e  ,   ;e  "   he  0   �e  %   �e     �e     �e     f     f     f     f  y  "f  �   �i  �   �j  y  ;k     �l  �   �l  L   �m  *   �m  O   n  &  in  !   �o  $   �o  p  �o  1  Hq     zr  	   r     �r     �r     �r  T   �r  @   s  1   Qs  $   �s     �s  �   �s  (   }t  *   �t  
   �t  8   �t     u     3u     Mu     ju  !   �u     �u  %   �u  #   �u  #   v  0   1v  )   bv     �v  	   �v     �v  
   �v  	   �v     �v  	   �v  D   �v  "   w     8w     =w     Dw  	   Rw     \w     cw               �                      �       �       �   �   �       t   *   �   �       �         �      Y   B   �   �   �   "   �       a   �         �   q   d           Z   �   �     S   f   	   [       �          �     �   �   T   �   7   �   �     8         _   9       1   �   5             �   �       .     3      {                             %  y     >   �   
  �       R       �   �   �       (      �   �   �           +          ^   �   U   "  �   �            �     �   u            �       �   �   �   !         W   \           �   �             C   K   �       }   �   �   �   �       �           �   �   �     �   j   F                     m   N   )       2           �         v              P   �   �   o     w       �   �   �             $  ~   �              @   ?        �   
           �   �   X   �   �   /   �   x      �           �      �           &  �         =       g   c   #  n   �   �      �   �      <   �   |   �   �       �     �   I   4   #      ,       �   �      O                   %   �   �       i       �   	              h   $       s       J   �   ]   �          &     �      �   �        �               �       �   :   �   0       `   k     p       �   �     L   H   �       �   E           �   �       �         V   �   �                 �   �   e   �       �   �   �   M        �   Q      �   r   A   6   D      �   l   �   �       '  b       G       �   ;           !           �   �   z   �   �   �   '   -   �   �   �       �   �            1. Turn radio off.
 2. Connect cable to DATA terminal.
 3. Press and hold in [DISP] key while turning on radio
      ("CLONE" will appear on radio LCD).
 4. Press [RECEIVE] screen button
      ("-WAIT-" will appear on radio LCD).
5. Finally, press OK button below.
 %(value)s must be between %(min)i and %(max)i %s has not been saved. Save before closing? (none) ...and %i more A new CHIRP version is available. Please visit the website as soon as possible to download it! About About CHIRP All All Files All supported formats| Amateur An error has occurred Applying settings Available modules Bandplan Bands Banks Bin Browser Building Radio Browser Canada Changing this setting requires refreshing the settings from the image, which will happen now. Channels with equivalent TX and RX %s are represented by tone mode of "%s" Chirp Image Files Choice Required Choose %s DTCS Code Choose %s Tone Choose Cross Mode Choose duplex Choose the module to load from issue %i: City Click on the "Special Channels" toggle-button of the memory
editor to see/set the EXT channels. P-VFO channels 100-109
are considered Settings.
Only a subset of the over 200 available radio settings
are supported in this release.
Ignore the beeps from the radio on upload and download.
 Clone completed, checking for spurious bytes Cloning Cloning from radio Cloning to radio Close Close file Comment Communicate with radio Complete Connect your interface cable to the PC Port on the
back of the 'TX/RX' unit. NOT the Com Port on the head.
 Convert to FM Copy Country Cross mode Custom Port Custom... Cut DTCS DTCS
Polarity DV Memory Danger Ahead Dec Delete Developer Mode Developer state is now %s. CHIRP must be restarted to take effect Diff Raw Memories Digital Code Digital Modes Disable reporting Disabled Distance Do not prompt again for %s Double-click to change bank name Download Download from radio Download from radio... Download instructions Dual-mode digital repeaters that support analog will be shown as FM Duplex Edit details for %i memories Edit details for memory %i Enable Automatic Edits Enabled Enter Frequency Enter Offset (MHz) Enter TX Frequency (MHz) Enter a new name for bank %s: Enter custom port: Erased memory %s Error applying settings Error communicating with radio Experimental driver Export can only write CSV files Export to CSV Export to CSV... Extra FREE repeater database, which provides most up-to-date
information about repeaters in Europe. No account is
required. Failed to load radio browser Failed to parse result Features File does not exist: %s Files Filter Filter results with location matching this string Find Find Next Find... Finished radio job %s Found empty list value for %(name)s: %(value)r Frequency GMRS Getting settings Goto Memory Goto Memory: Goto... Help Help Me... Hex Hide empty memories If set, sort results by distance from these coordinates Import Import from file... Import not recommended Index Info Information Insert Row Above Install desktop icon? Interact with driver Invalid %(value)s (use decimal degrees) Invalid Entry Invalid ZIP code Invalid edit: %s Invalid or unsupported module file Invalid value: %r Issue number: LIVE Latitude Length must be %i Limit Bands Limit Modes Limit Status Limit results to this distance (km) from coordinates Load Module... Load module from issue Load module from issue... Loading modules can be extremely dangerous, leading to damage to your computer, radio, or both. NEVER load a module from a source you do not trust, and especially not from anywhere other than the main CHIRP website (chirp.danplanet.com). Loading a module from another source is akin to giving them direct access to your computer and everything on it! Proceed despite this risk? Loading settings Longitude Memories Memory %i is not deletable Memory must be in a bank to be edited Memory {num} not in bank {bank} Mode Model Modes Module Module Loaded Module loaded successfully More than one port found: %s Move Down Move Up New Window New version available No empty rows below! No modules found No modules found in issue %i No results No results! Number Offset Only certain bands Only certain modes Only memory tabs may be exported Only working repeaters Open Open Recent Open Stock Config Open a file Open a module Open debug log Open in new window Open stock config directory Optional: -122.0000 Optional: 100 Optional: 45.0000 Optional: County, Hospital, etc. Overwrite memories? P-VFO channels 100-109 are considered Settings.
Only a subset of the over 130 available radio settings
are supported in this release.
 Parsing Paste Pasted memories will overwrite %s existing memories Pasted memories will overwrite memories %s Pasted memories will overwrite memory %s Pasted memory will overwrite memory %s Please be sure to quit CHIRP before installing the new version! Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio
3 - Click the button on this window to start download
    (you may see another dialog, click ok)
4 - Radio will beep and led will flash
5 - You will get a 10 seconds timeout to press "MON" before
    data upload start
6 - If all goes right radio will beep at end.
After cloning remove the cable and power cycle your radio to
get into normal mode.
 Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio.
3 - Click the button on this window to start download
    (Radio will beep and led will flash)
4 - Then press the "A" button in your radio to start cloning.
    (At the end radio will beep)
 Please note that developer mode is intended for use by developers of the CHIRP project, or under the direction of a developer. It enables behaviors and functions that can damage your computer and your radio if not used with EXTREME care. You have been warned! Proceed anyway? Please wait Plug in your cable and then click OK Port Power Press enter to set this in memory Print Preview Printing Properties Query %s Query Source RX DTCS Radio Radio did not ack block %i Radio information Radio sent data after the last awaited block, this happens when the selected model is a non-US but the radio is a US one. Please choose the correct model and try again. RadioReference Canada requires a login before you can query Refresh required Refreshed memory %s Reload Driver Reload Driver and File Rename bank Reporting enabled Reporting helps the CHIRP project know which radio models and OS platforms to spend our limited efforts on. We would really appreciate if you left it enabled. Really disable reporting? Restart Required Retrieved settings Save Save before closing? Save file Saved settings Security Risk Select Bandplan... Select Bands Select Modes Select a bandplan Service Settings Show Raw Memory Show debug log location Show extra fields Some memories are incompatible with this radio Some memories are not deletable Sort by column: Sort memories Sorting State State/Province Success The FT-450D radio driver loads the 'Special Channels' tab
with the PMS scanning range memories (group 11), 60meter
channels (group 12), the QMB (STO/RCL) memory, the HF and
50m HOME memories and all the A and B VFO memories.
There are VFO memories for the last frequency dialed in
each band. The last mem-tune config is also stored.
These Special Channels allow limited field editing.
This driver also populates the 'Other' tab in the channel
memory Properties window. This tab contains values for
those channel memory settings that don't fall under the
standard Chirp display columns. With FT450 support, the gui now
uses the mode 'DIG' to represent data modes and a new column
'DATA MODE' to select USER-U, USER-L and RTTY 
 The X3Plus driver is currently experimental.
There are no known issues but you should proceed with caution.
Please save an unedited copy of your first successful
download to a CHIRP Radio Images (*.img) file.
 The author of this module is not a recognized CHIRP developer. It is recommended that you not load this module as it could pose a security risk. Proceed anyway? The recommended procedure for importing memories is to open the source file and copy/paste memories from it into your target image. If you continue with this import function, CHIRP will replace all memories in your currently-open file with those in %(file)s. Would you like to open this file to copy/paste memories across, or proceed with the import? This Memory This driver has been tested with v3 of the ID-5100. If your radio is not fully updated please help by opening a bug report with a debug log so we can add support for the other revisions. This driver is in development and should be considered as experimental. This is an early stage beta driver
 This is an early stage beta driver - upload at your own risk
 This is experimental support for BJ-9900 which is still under development.
Please ensure you have a good backup with OEM software.
Also please send in bug and enhancement requests!
You have been warned. Proceed at your own risk! This memory and shift all up This memory and shift block up This radio has a tricky way of enter into program mode,
even the original software has a few tries to get inside.
I will try 8 times (most of the time ~3 will doit) and this
can take a few seconds, if don't work, try again a few times.
If you can get into it, please check the radio and cable.
 This should only be enabled if you are using modified firmware that supports wider frequency coverage. Enabling this will cause CHIRP not to enforce OEM restrictions and may lead to undefined or unregulated behavior. Use at your own risk! Tone Tone Mode Tone Squelch Tuning Step USB Port Finder Unable to determine port for your cable. Check your drivers and connections. Unable to edit memory before radio is loaded Unable to find stock config %r Unable to open the clipboard Unable to query Unable to read last block. This often happens when the selected model is US but the radio is a non-US one (or widebanded). Please choose the correct model and try again. Unable to reveal %s on this system Unable to set %s on this memory United States Unplug your cable (if needed) and then click OK Upload instructions Upload to radio Upload to radio... Uploaded memory %s Use fixed-width font Use larger font Value does not fit in %i bits Value must be at least %.4f Value must be at most %.4f Value must be exactly %i decimal digits Value must be zero or greater Values Vendor View WARNING! Warning Warning: %s Welcome Would you like CHIRP to install a desktop icon for you? Your cable appears to be on port: bits bytes bytes each disabled enabled {bank} is full Project-Id-Version: CHIRP
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-12-13 00:03+0100
Last-Translator: Matthieu Lapadu-Hargues <mlhpub@free.fr>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.2.2
  1. Eteignez la radio.
 2. Connectez le cable a la prise DATA.
 3. Appuyez et maintenez la touche [DISP] en allumant la radio
      ("CLONE" va apparaitre sur l'ecran LCD).
 4. Appuyez sur le bouton [RECEIVE] du logiciel
      ("-WAIT-" va apparaitre sur l'ecran LCD).
5. A la fin, pressez le bouton OK en-dessous.
 %(value)s doit etre entre %(min)i et %(max)i %s n'a pas ete enregistre. Enregistrer avant de fermer? (vide) ...et %i plus Une nouvelle version de CHIRP est disponible. Visitez le site internet des que possible pour la telecharger! A propos A propos de CHIRP Tout Tous fichiers Tours formats supportes| Amateur Une erreur s'est produite Application des preferences Modules disponibles Plan de frequences Bandes Banques Corbeille Navigateur Etablissement navigateur radio Canada Modifier ce parametre necessite de refraichir tous les parametres depuis l'image, ce qui va etre fait maintenant. Les canaux avec TX et RX equivalents %s sont representes par le mode de tonalite de "%s" Fichiers image Chirp Choix necessaire Choisir %s DTCS Code Choisir %s Tone Choisir cross mode Choisir duplex Choisir le module a charger depuis le probleme %i: Ville Cliquez sur le commutateur "Special Channels" de l'editeur de memoire 
pour voir ou modifier les canaux EXT. Les canaux P-VFO 100-109
sont consideres comme parametres.
Seul un sous-ensemble des 200 premiers parametres radio
disponibles sont suppportes dans cette publication.
Ne tenez pas compter des bips de la radio durant le telechargement.
 Clonage termine, verification des octets parasites Clonage Telechargement depuis la radio - Lire Telechargement vers la radio - Ecrire Fermer Fermer fichier Commentaire Communique avec la radio Termine Connectez votre cable d'interface au port PC a l'arriere
de l'unite 'TX/RX'. PAS au port de communication de la facade.
 Convertir en FM Copier Pays Cross mode Personnalisation du port Personnalisation... Couper DTCS DTCS
Polarity Memoire DV Danger devant Dec Supprimer Mode developpeur Le mode developpement est maintenant %s. CHIRP doit etre redemarre pour que ceci prenne effet Comparaison memoires brutes Digital Code Modes numeriques Desactiver le rapport d'utilisation Descative Distance Ne plus demander pendant %s Double-cliquer pour modifier le nom de la banque Telecharger Telecharger depuis la radio - Lire Telecharger depuis la radio - Lire... Instructions de telechargement Les repeteurs numeriques bimodes supportant l'analogique seront presentes comme FM Duplex Editer les details pour %i memoires Editer les details pour la memoire %i Aciter l'edition automatique Active Entrer la frequence Saisir le decalage (MHz) Saisir la frequence d'emission (MHz) Saisir un nouveau nom pour la banque %s: Saisir un port personnalise: Effacer la memoire %s Erreur lors de l'application des preferences Erreur de communication avec la radio Pilote experimental L'exportation ne peut ecrire que des fichiers CSV Exporter vers un fichier CSV Exporter vers un fichier CSV... Extra Base de donnees LIBRE de relais proposant les informations
les plus a jour sur les relais Europeens. Un compte utiliseteur
n'est pas requis. Echec du chargement du navigateur de la radio Echec d'analyse des resultats Caracteristiques Fichier inexistant: %s Fichiers Filtrer Filtrer les resultats avec emplacement correspondant a cette chaine de caracteres Rechercher Rechercher le suivant Rechercher... Tache radio terminee %s Liste de valeurs vide trouvee pour %(name)s: %(value)r Frequence GMRS Acquisition des preferences Aller a la memoire Aller a la memoire: Aller... Aide Aidez moi... Hex Cacher les memoires vides Si selectionne, trier les resultats selon la distance depuis ces coordonnees Importer Importer depuis un fichier... Import non recommande Index Info Information Inserer une ligne avant Installer une icone sur le bureau? Interaction avec le pilote Invalide %(value)s (utiliser degres decimaux) Entree invalide Code postal invalide Edition invalide: %s Fichier module invalide ou pas supporte Valeur invalide: %r Probleme numero: LIVE Latitude La longueur doit etre %i Limite les bandes Limite les modes Limites les etats Limite les resultats a cette distance (km) depuis les coordonnees Chargement module... Chargement module depuis un probleme Chargement module depuis un probleme... Charger des modules peut etre extremement dangereux, causant des dommages a votre ordinateur, radio ou les deux. NE JAMAIS charger un module issu d'une source dont vous n'avez pas confiance, et plus specialement d'ailleurs que du site internet principal de CHIRP (chirp.danplanet.com). Charger un module depuis une autre source revient a donner un acces direct a votre ordinateur et a tout ce qu'il contient! Continuez malgre le risque? Chargement des preferences Longitude Memoires La memoire %i n'est pas supprimable La memoire doit entre dans une banque pour etre editee Memoire {num} pas dans la banque {bank} Mode Modele Modes Module Module charge Module charge avec succes Plus d'un port trouve: %s Descendre Monter Nouvelle fenetre Nouvelle version disponible Pas de ligne vide dessous! Aucun module trouve Aucun module trouve pour le probleme %i Aucun resultat Aucun resultat! Nombre Decalage Seulement certaines bandes Seulement certains modes Seuls les onglets des memoires peuvent etre exportes Uniquement les repeteurs en fonctionnement Ouvrir Ouvrir recent Ouvrir base de donnees Ouvrir un fichier Ouvrir un module Ouvrir le fichier de debogage Ouvrir dans une nouvelle fenetre Ouvrir le repertoire de base de donnees Optionnel: -122.0000 Optionnel: 100 Optionnel: 45.0000 Optionnel: County, Hospital, etc. Ecraser les memoires? Les canaux P-VFO 100-109 sont considere comme parametres.
Seul un sous-ensemble des plus de 130 parametres radio disponibles
est pris en charge dans cette version.
 Analyse Coller Les memoires collees vont ecraser %s memoires existantes Les memoires collees vont ecraser les memoires %s Les memoires collees vont ecraser la memoire %s La memoire collee va ecraser la memoire %s Soyez certain de quitter CHIRP avant d'installer la nouvelle version! Suiez ces etapes avec attention:
1 - Allumez votre radio
2 - Connectez le cable d'interface a votre radio
3 - Cliquez sur le bouton de cette fenetre pour demarrer le telechargement
    (vous devriez voir une autre boite de dialogue, cliquez sur ok)
4 - La radio va sonner et la LED clignoter
5 - Vous disposez de 10 secondes pour appuyer sur "MON" avant que
    le telechargement vers la radio ne demarre
6 - Si tout se deroule bien la radio va sonner a la fin.
Apres le clonage debranchez le cable puis eteignez et rallumez votre radio
pour revenir au fonctionnement normal.
 Suiez ces etapes avec attention:
1 - Allumez votre radio
2 - Connectez le cable d'interface a votre radio
3 - Cliquez sur le bouton de cette fenetre pour demarrer le telechargement
    (La radio va sonner et la LED clignoter)
4 - Ensuite appuyez sur le bouton "A" de votre radio pour demarrer le clonage
    (A la fin la radio va sonner)
 Veuillez noter que le mode developpeur est destiné a n'etre utilise que par les développeurs du projet CHIRP, ou en suivant les directives d'un developpeur. Il active des comportements et des fonctions qui peuvent endommager votre ordinateur et votre radio s'ils ne sont pas utilises avec un EXTREME soin. Vous etes prevenu! Continuer malgré tout? Patientez s'il vous plait Branchez votre cable et cliquez sur OK Port Puissance Appuyez sur entree pour inscrire ceci en memoire Apercu avant impression Impression Proprietes Interroger %s Interroger Source RX DTCS Radio La radio n'a pas accuse reception du bloc %i Informations de la radio La radio a envoye des donnees apres le dernier bloc attendu, ceci se produit quand le modele selectionne n'est pas US mais que la radio est US. Selectionnez le bon modele et reessayez. RadioReference Canada necessite une connexion avant une requete Rafraichissement necessaire Rafraichir la memoire %s Recharger pilote Recharger pilote et fichier Renommer banque Rapport d'utilisation active Le rapport d'utilisation aide le projet CHIRP a savoir quelles radios et systemes d'exploitation sont utilises pour concentrer les efforts sur ceux-la. Nous apprecierions vraiment que vous le laissiez actif. Etes-vous certain de vouloir desactiver le rapport d'utilisation? Redemarrage necessaire Preferences recuperees Enregistrer Enregistrer avant de fermer? Enregistrer fichier Preferences enregistrees Risque de securite Choix d'un plan de frequences... Choix des bandes Choix des modes Choix d'un plan de frequences Service Preferences Afficher les donnees de memoires brutes Montrer l'emplacement du fichier de debogage Montrer les champs supplementaires Des memoires sont incompatibles avec cette radio Des memoires ne sont pas supprimables Tri par colonne: Tri des memoires Tri Etat Etat/Province Reussi Le pilote du FT-450D charge l'onglet des "Special Channels'
avec les memoires de plage de balayage PMS (groupe 11), les canaux
60 metres (groupe 12), la memoire QMB (STO/RCL), les memoires HOME HF et
50 m et toutes les memoires VFO A et B.
Il y a des memoires VFO pour les dernières fréquences regles sur
chaque bande. La derniere configuration de reglage de memoire est egalement stockee.
Ces canaux spéciaux permettent une edition reduite des champs.
Ce pilote remplit egalement l'onglet "Other" de la fenetre
Propriétés du canal mémoire. Cet onglet contient des valeurs pour
les parametres de canal memoire qui ne correspondent pas aux colonnes d'affichage
Chirp standard. Avec la prise en charge du FT450, l'interface graphique utilise
desormais le mode 'DIG' pour représenter les modes de données et une nouvelle colonne
'DATA MODE' pour sélectionner USER-U, USER-L et RTTY 
 Le pilote X3Plus est actuellement experimental.
Il n'y a pas de probleme connu mais vous devriez proceder avec precaution.
Sauvegardez une copie non editee de votre premier
telechargement dans un fichier image radio CHIRP (*.img).
 L’auteur de ce module n'est pas un developpeur de CHIRP connu. Comme il peut comporter un risque de securite, il est recommande de ne pas importer ce module. Importer tout de meme? La procedure d'importation des memoires preconisee consiste a ouvrir le fichier source et copier/coller les memoires depuis celui-ci vers l'image cible. Si vous continuez avec cette fonction d'importation, CHIRP va remplacer toutes les memoires du fichier actuellement ouvert avec celles de %(file)s. Souhaitez-vous ouvrir ce fichier pour copier/coller entre eux ou l'importer? Cette memoire Ce pilote a ete teste avec la v3 de l'ID-5100. Si votre radio n'est pas a jour merci d'aider en ouvrant un rapport de bogue avec un journal de debogage pour que nous puissions ajouter du support pour les autres revisions. Ce pilote est en developpement et devrait etre considere comme experimental. Ceci est un pilote recent en version beta
 Ceci est un pilote recent en version beta - chargement a vos risques et perils
 Ceci est un support experimental pour le BJ-9900 encore en cours de developpement.
SVP assurez-vous de disposer d'une sauvegarde correcte avec le logiciel d'origine (OEM).
Envoyez aussi vos demandes d'ameliorations ou rapports de bogues!
Vous avez ete prevenu. Procedez a vos risques et perils! Remonter ces memoires et decalage Bloquer cette memoire et le decalage Cette radio rentre en mode programmation avec une methode particuliere,
meme le logiciel d'origine fait plusieurs tentative pour y parvenir.
Je vais essayer 8 fois (la plupart du temps ~3 fois suffisent) et cela pourra
prendre quelques secondes, si cela ne fonctionne pas, essayez encore quelques fois.
Si vous ne parvenez pas a entrer, verifiez la radio et le cable.
 Ceci ne devrait etre active que si vous utilisez un micrologiciel modifie supportant une plus large couverture en frequences. L'activation de cette option empechera CHIRP d'appliquer les restrictions OEM et peut conduire a un comportement indefini ou non reglementaire. A utiliser a vos risques et perils! Tone Tone Mode Tone Squelch Pas de reglage Recherche de port USB Impossible de determiner le port de votre cable. Verifiez les pilotes et connexions. Impossible d'editer une memoire avant telechargement de la radio Impossible de trouver la configuration de base %r Impossible d'ouvrir le presse-papier Impossible d'interroger Impossible de lire le dernier bloc. Ceci se produit frequemment quand le modele selectionne est US mais que la radio n'est pas US (ou large-bande). Selectionnez le bon modele et reessayez. Impossible de montrer %s dans ce systeme Impossible de regler %s dans cette memoire Etats-Unis Debranchez votre cable (si necessaire) et cliquez sur OK Instruction de telechargement Telecharger vers la radio Telecharger vers la radio... Memoire telechargee %s Utiliser une police a chasse fixe Utiliser une plus grande police La valeur ne correspond pas a %i bits La valeur doit etre d'au moins %.4f La valeur ne doit pas depasser %.4f La valeur doit comporter exactement %i decimales Les valeur doivent etre zero ou positives Valeurs Fabricant Voir ATTENTION! Attention Attention: %s Bienvenue Souhaitez-vous que CHIRP installe une icone pour vous sur le bureau? Votre cable apparai
t sur le port: bits octets octets chacun desactive active {bank} pleine 